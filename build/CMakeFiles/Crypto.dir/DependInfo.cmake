# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/andrewche/DoshiCoin/src/crypto/blake256.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/blake256.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/chacha8.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/chacha8.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/crypto-ops-data.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/crypto-ops-data.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/crypto-ops.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/crypto-ops.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/groestl.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/groestl.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/hash-extra-blake.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/hash-extra-blake.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/hash-extra-groestl.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/hash-extra-groestl.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/hash-extra-jh.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/hash-extra-jh.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/hash-extra-skein.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/hash-extra-skein.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/hash.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/hash.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/jh.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/jh.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/keccak.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/keccak.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/oaes_lib.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/oaes_lib.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/random.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/random.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/skein.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/skein.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/slow-hash.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/slow-hash.c.o"
  "/home/andrewche/DoshiCoin/src/crypto/tree-hash.c" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/tree-hash.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../include"
  "../../src"
  "../../external"
  "version"
  "../../src/Platform/Linux"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/andrewche/DoshiCoin/src/crypto/crypto.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/crypto.cpp.o"
  "/home/andrewche/DoshiCoin/src/crypto/slow-hash.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Crypto.dir/crypto/slow-hash.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../../include"
  "../../src"
  "../../external"
  "version"
  "../../src/Platform/Linux"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
