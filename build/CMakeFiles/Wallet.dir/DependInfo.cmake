# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/andrewche/DoshiCoin/src/Wallet/LegacyKeysImporter.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/Wallet/LegacyKeysImporter.cpp.o"
  "/home/andrewche/DoshiCoin/src/Wallet/WalletAsyncContextCounter.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/Wallet/WalletAsyncContextCounter.cpp.o"
  "/home/andrewche/DoshiCoin/src/Wallet/WalletErrors.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/Wallet/WalletErrors.cpp.o"
  "/home/andrewche/DoshiCoin/src/Wallet/WalletGreen.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/Wallet/WalletGreen.cpp.o"
  "/home/andrewche/DoshiCoin/src/Wallet/WalletRpcServer.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/Wallet/WalletRpcServer.cpp.o"
  "/home/andrewche/DoshiCoin/src/Wallet/WalletSerialization.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/Wallet/WalletSerialization.cpp.o"
  "/home/andrewche/DoshiCoin/src/Wallet/WalletUtils.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/Wallet/WalletUtils.cpp.o"
  "/home/andrewche/DoshiCoin/src/WalletLegacy/KeysStorage.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/WalletLegacy/KeysStorage.cpp.o"
  "/home/andrewche/DoshiCoin/src/WalletLegacy/WalletHelper.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletHelper.cpp.o"
  "/home/andrewche/DoshiCoin/src/WalletLegacy/WalletLegacy.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletLegacy.cpp.o"
  "/home/andrewche/DoshiCoin/src/WalletLegacy/WalletLegacySerialization.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletLegacySerialization.cpp.o"
  "/home/andrewche/DoshiCoin/src/WalletLegacy/WalletLegacySerializer.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletLegacySerializer.cpp.o"
  "/home/andrewche/DoshiCoin/src/WalletLegacy/WalletTransactionSender.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletTransactionSender.cpp.o"
  "/home/andrewche/DoshiCoin/src/WalletLegacy/WalletUnconfirmedTransactions.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletUnconfirmedTransactions.cpp.o"
  "/home/andrewche/DoshiCoin/src/WalletLegacy/WalletUserTransactionsCache.cpp" "/home/andrewche/DoshiCoin/build/release/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletUserTransactionsCache.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../../include"
  "../../src"
  "../../external"
  "version"
  "../../src/Platform/Linux"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
